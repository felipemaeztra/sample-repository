# Trabalhando com git flow
Este repositório propõe uma documentação mais pratica e explicativa, refletido em processos internos do que a [documentação original](https://danielkummer.github.io/git-flow-cheatsheet/index.pt_BR.html).
Se você não está familiarizado com os comandos básicos do git flow, recomendo que a leitura seja acompanhada da [documentação](https://danielkummer.github.io/git-flow-cheatsheet/index.pt_BR.html).

Toda as etapas seguirão o fluxo de trabalho do git flow e também indicará o responsável por aquela ação, ou seja, você saberá qual profissional é responsável por uma abertura ou encerramento de determinada branch. Além disso também trará uma tabela traduzindo o **git flow** em **git raw** para termos uma ideia do que está sendo executado pelo CLI do git flow.

#### Considerações importantes sobre o git flow:
 1. O git flow não envia para o repositório remoto, esse comando é feito no bom é velho git, ou seja, **TODOS** os comandos do git flow realizaram apenas ações **LOCAIS**.
 2. O git flow não dita que determinada tarefa é uma feature, um bugfix ou hotfix, isso deve ser decido no briefing, no cadastro da tarefa ou pela regra governamental do TI.

## Iniciando
Um repositório deve ser iniciado e configurado pelo Líder do projeto ou do time, por exemplo. 

Veja quais ações o git flow realiza nesta etapa:

gitflow | git
--------|-----
`git flow init` | `git init`
&nbsp; | `git commit --allow-empty -m "Initial commit"`
&nbsp; | `git checkout -b develop master`


## Conectando ao repositório remoto
Como citado nas considerações, o **git flow** não envia nada para o repositório remoto, portanto essa etapa é realizado com **git raw**.

gitflow | git
--------|-----
_N/A_ | `git remote add origin git@github.com:MYACCOUNT/MYREPO`


## Features
Assim como mencionado nas considerações *o que é* uma feature não é ditada pelo git flow, uma alteração pode ser uma feature, assim como algo completamente novo também.
Mas vamos assumir que é uma feature:
 - Algo novo, um novo endpoint da API, uma nova página no site ou qualquer coisa que não existia;
 - Uma alteração em algo que **ESTÁ FUNCIONANDO**, como por exemplo trocar o texto no HTML, melhorar uma validação de determinado script, ou substituir um plugin.

#### Quem executa essas ações?
Todas as ações em branches de feature podem ser realizadas pelo desenvolvedor responsável pela execução da tarefa, desde seu inicio até o merge na branch develop.

### Criando uma *branch* de *feature* 
Criar a branch é o primeiro passo, o git flow criar essa estrutura localmente para você.

gitflow | git
--------|-----
`git flow feature start MYFEATURE` | `git checkout -b feature/MYFEATURE develop`


### Compartilhar uma branch em andamento
Em alguns casos podemos trabalhar juntos em uma mesma funcionalidade, portanto aquela branch que você acabou de criar deve ser disponibilizada para o seu colega.

gitflow | git
--------|-----
`git flow feature publish MYFEATURE` | `git checkout feature/MYFEATURE`
&nbsp; | `git push origin feature/MYFEATURE`


### Atualizar versão local de uma feature
Uma feature compartilhada, muito provavelmente, receberá commits dos seus colegas, portanto é importante atualizar sua versão sempre antes de começar a trabalhar nela.
gitflow | git
--------|-----
`git flow feature pull origin MYFEATURE` | `git checkout feature/MYFEATURE`
&nbsp; | `git pull --rebase origin feature/MYFEATURE`


### Finalizar uma feature
Quando uma feature é finalizada e está pronta para ser testada ela pode sofrer um *merge* na branch *develop*.
Isso apagara a branch atual, tornando ela indisponível no repositório.

gitflow | git
--------|-----
`git flow feature finish MYFEATURE` | `git checkout develop`
&nbsp; | `git merge --no-ff feature/MYFEATURE`
&nbsp; | `git branch -d feature/MYFEATURE`


### Enviar a feature finalizada para o repositório remoto
Como já sabemos, o git flow não se comunica com o repositório remoto, portanto você fará isso com **git raw**.

gitflow | git
--------|-----
_N/A_ | `git push origin develop`
&nbsp; | `git push origin :feature/MYFEATURE` _(if pushed)_

## Bugfixes
No git flow existe a branch do tipo `bugfix`, ela categoriza principalmente correções em features que já sofreram o merge com a `branch develop`.
Um bugfix pode ser uma branch gerada a partir de uma branch de `feature` que está aberta e então ter seu merge feito nesta branch em questão, porém isso é determinado pela necessidade da demanda e organização da governança de TI.

#### Quem executa essas ações?
O desenvolvedor é o responsável pela abertura e finalização de um bugfix.

### Criando uma bugfix
A bugfix é semelhante a uma feature em termos de execução:

gitflow | git
--------|-----
`git flow bugfix start NAME` | `git checkout -b develop/NAME develop`

### Compartilhar uma branch em andamento
Semelhante as demais, para disponibilizar a branch no repositório remoto basta:

gitflow | git
--------|-----
`git flow bugfix publish NAME` | `git checkout bugfix/NAME`
&nbsp; | `git push origin bugfix/NAME`

### Finalizar uma bugfix
Quando uma bugifx é finalizada e está pronta para ser testada ela pode sofrer um *merge* na branch *develop*.
Isso apagara a branch atual, tornando ela indisponível no repositório.

gitflow | git
--------|-----
`git flow feature finish NAME` | `git checkout develop`
&nbsp; | `git merge --no-ff bugfix/NAME`
&nbsp; | `git branch -d bugfix/NAME`


### Enviar a bugfix finalizada para o repositório remoto
Como já sabemos, o git flow não se comunica com o repositório remoto, portanto você fará isso com **git raw**.

gitflow | git
--------|-----
_N/A_ | `git push origin develop`
&nbsp; | `git push origin :bugfix/NAME` _(if pushed)_


## Releases
Diferente de uma feature, uma release tem seu valor muito mais explicito. A release é o grupo de alterações que juntas são significativas para aplicação, isso não está relacionado a quantidade de código ou funcionalidades. A release é uma versão **estável** com correções e melhorias comparado a versão anterior, ou seja, qualquer coisa pode compor uma release.

Em termos mais práticos, uma release pode ser determinada pelo líder do time de desenvolvimento sendo o resultado de uma sprint. Em outro cenário uma release pode ser determinada pelo cliente, sendo ela o resultado de diversas alterações e correções que o cliente pediu e serão entregues ao final das sprints necessárias para a execução de todas as tarefas.

#### Quem executa essas ações?
Esta etapa exige a atenção do Líder, é o momento dos testes finais e deploy em ambiente de testes para o QA do cliente.

### Criar uma release
Seguindo a mesma estrutura dos demais comandos, você terá:
 `git flow [branch] start [name]`
 Contudo, nesta etapa o nome da branch é, necessariamente, o titulo da versão, normalmente recebendo números correspondendo: `[major].[minor].[path]`
 1. versão Maior(MAJOR): quando fizer mudanças que gerem incompatibilidades,
2. versão Menor(MINOR): quando adicionar funcionalidades mantendo a compatibilidade, e
3. versão de Correção(PATCH): quando corrigir falhas mantendo compatibilidade.

É recomendado que o nome da versão siga as regras no versionamento semático.

gitflow | git
--------|-----
`git flow release start 1.2.0` | `git checkout -b release/1.2.0 develop`


### Compartilhar uma release
Seguindo o padrão do git flow, compartilhamos a branch, disponibilizando ela no repositório remoto.

gitflow | git
--------|-----
`git flow release publish 1.2.0` | `git checkout release/1.2.0`
&nbsp; | `git push origin release/1.2.0`


### Atualizar versão local de uma release
Para atualizar a versão atual de uma release já existente, novamente, o git flow não interage com o repositório remoto, portanto:

gitflow | git
--------|-----
_N/A_ | `git checkout release/1.2.0`
&nbsp; | `git pull --rebase origin release/1.2.0`


### Finalizar uma release
Ao finalizar uma release, você estará realizando o merge na branch master.

gitflow | git
--------|-----
`git flow release finish 1.2.0` | `git checkout master`
&nbsp; | `git merge --no-ff release/1.2.0`
&nbsp; | `git tag -a 1.2.0`
&nbsp; | `git checkout develop`
&nbsp; | `git merge --no-ff release/1.2.0`
&nbsp; | `git branch -d release/1.2.0`


### Enviar alterações para o repositório remoto
No final do fluxo de uma feature até o ambiente de produção é necessário enviar todos as alterações para o repositório remoto e isso é realizado da forma tradicional:

gitflow | git
--------|-----
_N/A_ | `git push origin master`
&nbsp; | `git push origin develop`
&nbsp; | `git push origin --tags`
&nbsp; | `git push origin :release/1.2.0` _(if pushed)_


## Hotfixes
Uma hotfix é a correção emergencial de algo, esta branch terá seu merge feito diretamente na master.
#### Quem executa essas ações?
A abertura e finalização dessa branch pode ser feita pelo desenvolvedor, contudo é altamente recomendado que o Líder realize a ação da finalizar e realizar o merge.

### Criar uma hotfix
Uma hotfix se inicia igual as demais branches no git flow, porém o nome recebe um tratamento especial, ele será a versão desta correção, portanto se nossa aplicação estava na versão 1.2.0, esse hotfix será o 1.2.1.

gitflow | git
--------|-----
`git flow hotfix start 1.2.1 [commit]` | `git checkout -b hotfix/1.2.1 [commit]`


### Finalizar uma hotfix 
Ao finalizar uma hotfix ela é mesclada tanto na develop quanto na master. Além disso, o merge na master é etiquetado.

gitflow | git
--------|-----
`git flow hotfix finish 1.2.1` | `git checkout master`
&nbsp; | `git merge --no-ff hotfix/1.2.1`
&nbsp; | `git tag -a 1.2.1`
&nbsp; | `git checkout develop`
&nbsp; | `git merge --no-ff hotfix/1.2.1`
&nbsp; | `git branch -d hotfix/1.2.1`


### Enviar a hotfix para o repositório remoto
Como sabemos, o git flow não interage como repositório remoto, portanto enviamos as alterações manualmente.

gitflow | git
--------|-----
_N/A_ | `git push origin master`
&nbsp; | `git push origin develop`
&nbsp; | `git push origin --tags`
&nbsp; | `git push origin :hotfix/1.2.1` _(if pushed)_

## Referências

 - http://nvie.com/posts/a-successful-git-branching-model/
 - https://help.github.com/articles/using-pull-requests#shared-repository-model
 - https://semver.org/lang/pt-BR/